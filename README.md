## Build docker image

```
docker build . 
```


## Run docker container

```
docker run -p 26900:26900/udp -p 27020:27020/udp -p 27015:27015/udp -p 27015:27015 -d \
        -e MAXPLAYERS=32 \
        -e START_MAP=fy_tuscan \
        -e SERVER_NAME="Mirabo" \
        -e SERVER_PASSWORD="mirabo2020" \
        -e RCON_PASSWORD="mirabo2020" \
        -e START_MONEY=16000 \
        -e BUY_TIME=0.25 \
        -e FRIENDLY_FIRE=1 \
        -e ADMIN_STEAM=0:1:1234566 \
        --name cs \
        mirabo/cs16_server:1.0 +log
```

## Connect from CS client

```
connect <<ip_address>>:27015; password mirabo2020
```
